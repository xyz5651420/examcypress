describe("Sigin test",()=>{
  beforeEach(()=>{
   cy.visit("https://www.saucedemo.com/")
   
  })
  it('Check for page elements',()=>{
    cy.get('[data-test="username"]').should("be.visible")
    cy.get('[data-test="password"]').should("be.visible")
    cy.get('[data-test="login-button"]').should("be.visible")
  })

  it("Successful Signin",()=>{
    cy.get('[data-test="username"]').type("standard_user")
    cy.get('[data-test="password"]').type("secret_sauce")
    cy.get('[data-test="login-button"]').click()
    cy.get('[data-test="title"]').should('be.visible')
  })

  it("Unsuccessful Signin with unregistered user",()=>{
    cy.get('[data-test="username"]').type("user")
    cy.get('[data-test="password"]').type("secret_sauce")
    cy.get('[data-test="login-button"]').click()
    cy.get('[data-test="error"]').should('contain','Epic sadface: Username and password do not match any user in this service')
  })

  it("Unsuccessful Sigin without entering username",()=>{

    cy.get('[data-test="password"]').type("secret_sauce")
    cy.get('[data-test="login-button"]').click()
    cy.get('[data-test="error"]').should('contain','Epic sadface: Username is required')
  })

  it("Unsuccessful Sigin without entering password",()=>{

    cy.get('[data-test="username"]').type("standard_user")
    cy.get('[data-test="login-button"]').click()
    cy.get('[data-test="error"]').should('contain','Epic sadface: Password is required')
  })

  it("Unsuccessful Sigin without entering username and password",()=>
  {
    
    cy.get('[data-test="login-button"]').click()
    cy.get('[data-test="error"]').should('contain','Epic sadface: Username is required')
  })

  it("Signin with locked out user",()=>{
    cy.get('[data-test="username"]').type("locked_out_user")
    cy.get('[data-test="password"]').type("secret_sauce")
    cy.get('[data-test="login-button"]').click()
    cy.get('[data-test="error"]').should('contain','Epic sadface: Sorry, this user has been locked out.')
  })

  it("Sigin with pressing enter key of keyboard",()=>{
    cy.get('[data-test="username"]').type("standard_user")
    cy.get('[data-test="password"]').type("secret_sauce{enter}")
    cy.contains("Products")
    cy.url().should('eq','https://www.saucedemo.com/inventory.html')
  })
  })