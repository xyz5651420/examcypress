describe("Checkout from Cart",()=>{
  beforeEach(()=>{
    cy.visit("https://www.saucedemo.com/")
    cy.get('[data-test="username"]').type("standard_user")
    cy.get('[data-test="password"]').type("secret_sauce")
    cy.get('[data-test="login-button"]').click()
    cy.get('[data-test="add-to-cart-sauce-labs-backpack"]').click()
    cy.get('[data-test="add-to-cart-sauce-labs-bike-light"]').click()
    cy.get('[data-test="add-to-cart-sauce-labs-bolt-t-shirt"]').click()
    cy.scrollTo('top')
    cy.get('[data-test="shopping-cart-link"]').click()
  })
  
  it("Successful Checkout",()=>{
    cy.scrollTo('bottom')
    cy.get('[data-test="checkout"]').click()
    cy.scrollTo('top')
    cy.get('[data-test="firstName"]').type("Blue")
    cy.get('[data-test="lastName"]').type("Lemonade")
    cy.get('[data-test="postalCode"]').type("56613")
    cy.get('[data-test="continue"]').click()
    cy.scrollTo('bottom')
    cy.get('[data-test="finish"]').click()
    cy.get('[data-test="back-to-products"]').click()
  })

  it("Checkout without entering first name",()=>{
    cy.scrollTo('bottom')
    cy.get('[data-test="checkout"]').click()
    cy.scrollTo('top')
    cy.get('[data-test="lastName"]').type("Lemonade")
    cy.get('[data-test="postalCode"]').type("56613")
    cy.get('[data-test="continue"]').click()
    cy.get('.error-message-container').should('contain','Error: First Name is required')
  })

  it("Checkout without entering last name",()=>{
    cy.scrollTo('bottom')
    cy.get('[data-test="checkout"]').click()
    cy.scrollTo('top')
    cy.get('[data-test="firstName"]').type("Yellow")
    cy.get('[data-test="postalCode"]').type("56613")
    cy.get('[data-test="continue"]').click()
    cy.get('.error-message-container').should('contain','Error: Last Name is required')
  })

  it("Checkout without entering postal code",()=>{
    cy.scrollTo('bottom')
    cy.get('[data-test="checkout"]').click()
    cy.scrollTo('top')
    cy.get('[data-test="firstName"]').type("Blue")
    cy.get('[data-test="lastName"]').type("Lemonade")
    cy.get('[data-test="continue"]').click()
    cy.get('.error-message-container').should('contain','Error: Postal Code is required')
  })

  it("Cancel checkout",()=>{
    cy.scrollTo('bottom')
    cy.get('[data-test="checkout"]').click()
    cy.scrollTo('top')
    cy.get('[data-test="firstName"]').type("Blue")
    cy.get('[data-test="lastName"]').type("Lemonade")
    cy.get('[data-test="postalCode"]').type("56613")
    cy.get('[data-test="cancel"]').click()
  })

  it("Remove item during checkout",()=>{
    cy.get('[data-test="remove-sauce-labs-bike-light"]').click()
    cy.get('[data-test="shopping-cart-badge"]').should('have.text', '2')
  })

  it("Remove multiple items during checkout",()=>{
    cy.get('[data-test="remove-sauce-labs-bike-light"]').click()
    cy.get('[data-test="remove-sauce-labs-backpack"]').click()
    cy.get('[data-test="shopping-cart-badge"]').should('have.text', '1')
  })

  it("Remove all items during checkout",()=>{
    cy.get('[data-test="remove-sauce-labs-bike-light"]').click()
    cy.get('[data-test="remove-sauce-labs-backpack"]').click()
    cy.get('[data-test="remove-sauce-labs-bolt-t-shirt"]').click()
    cy.get('[data-test="shopping-cart-badge"]').should('not.exist')
  })

  it("Checkout with no items in cart",()=>{
    cy.get('[data-test="remove-sauce-labs-bike-light"]').click()
    cy.get('[data-test="remove-sauce-labs-backpack"]').click()
    cy.get('[data-test="remove-sauce-labs-bolt-t-shirt"]').click()
    cy.get('[data-test="checkout"]').click()
    cy.contains("Your Cart")
    //checkout shouldn't be possible but this website allows to checkout with no items
  })





})