describe("Add to cart test",()=>{
  beforeEach(()=>{
    cy.visit("https://www.saucedemo.com/")
    cy.get('[data-test="username"]').type("standard_user")
    cy.get('[data-test="password"]').type("secret_sauce")
    cy.get('[data-test="login-button"]').click()
  })
  it('sort by Name(Z to A)',()=>{
    cy.get('[data-test="product-sort-container"]').select("Name (Z to A)")
    cy.get(':nth-child(1) > [data-test="inventory-item-description"]').should('contain','Test.allTheThings() T-Shirt (Red)')
    cy.get(':nth-child(2) > [data-test="inventory-item-description"]').should('contain','Sauce Labs Onesie')
  })

  it('sort by Name(A to Z) from Name(Z to A)',()=>{
    cy.get('[data-test="product-sort-container"]').select("Name (Z to A)")
    cy.get(':nth-child(1) > [data-test="inventory-item-description"]').should('contain','Test.allTheThings() T-Shirt (Red)')
    cy.get(':nth-child(2) > [data-test="inventory-item-description"]').should('contain','Sauce Labs Onesie')

    cy.get('[data-test="product-sort-container"]').select("Name (A to Z)")
    cy.get(':nth-child(1) > [data-test="inventory-item-description"]').should('contain','Sauce Labs Backpack')
    cy.get(':nth-child(2) > [data-test="inventory-item-description"]').should('contain','Sauce Labs Bike Light')
  })

  it('sort by Price (low to high) from Name(A to Z)',()=>{
    cy.get('[data-test="product-sort-container"]').select("Price (low to high)")
    cy.get(':nth-child(1) > [data-test="inventory-item-description"]').should('contain','$7.99')
    cy.get(':nth-child(2) > [data-test="inventory-item-description"]').should('contain','$9.99')
  })

  it('sort by Price (high to low) from Name(A to Z)',()=>{
    cy.get('[data-test="product-sort-container"]').select("Price (high to low)")
    cy.get(':nth-child(1) > [data-test="inventory-item-description"]').should('contain','$49.99')
    cy.get(':nth-child(2) > [data-test="inventory-item-description"]').should('contain','$29.99')
  })
})